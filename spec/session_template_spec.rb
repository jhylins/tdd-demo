require './lib/bakesale_mar9.rb'

# rspec is the language used to write tests for code written in ruby.
# Tests follow this format:

#   describe 'a thing' do
#
#     it 'does something' do
#         expect(runTheThing(inputValue)).to eq(expectedResult)
#     end
#
#   end

describe 'calculateAmountToPay' do
   it 'charges x for y cakes' do
       expect(calculateAmountToPay(x)).to eq(y)
   end
    
end