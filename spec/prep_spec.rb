require './lib/prep.rb'

describe 'calculateTotal' do
    it "gives a total of 1 when 1 cake is bought" do
        expect(calculateTotal(1)).to eq 1
    end 

    it "gives a total of 2 when 2 cakes are bought" do
        expect(calculateTotal(2)).to eq 2
    end 
    it "gives a total of 2 when 3 cakes are bought" do
        expect(calculateTotal(3)).to eq 2
    end 
    it "gives a total of 3 when 5 cakes are bought" do
        expect(calculateTotal(5)).to eq 3
    end 
    it "gives a total of 3 when 4 cakes are bought" do
        expect(calculateTotal(4)).to eq 3
    end 
end