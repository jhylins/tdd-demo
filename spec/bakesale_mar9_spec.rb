require './lib/bakesale_mar9.rb'

# rspec is the language used to write tests for code written in ruby.
# Tests follow this format:

#   describe 'a thing' do
#
#     it 'does something' do
#         expect(runTheThing(inputValue)).to eq(expectedResult)
#     end
#
#   end

describe 'calculateAmountToPay' do
   it 'charges £1 for 1 cake' do
       expect(calculateAmountToPay(1)).to eq(1)
   end

   it 'charges £2 for 2 cakes' do
       expect(calculateAmountToPay(2)).to eq(2)
   end

   it 'applies a 3 for 2 offer' do
       expect(calculateAmountToPay(3)).to eq(2)
       expect(calculateAmountToPay(6)).to eq(4)
   end

   it 'charges £1 for each extra cake after offers applied' do
       expect(calculateAmountToPay(4)).to eq(3)
       expect(calculateAmountToPay(7)).to eq(5)
       expect(calculateAmountToPay(11)).to eq(7)
       expect(calculateAmountToPay(13)).to eq(8)
       expect(calculateAmountToPay(14)).to eq(9)
       expect(calculateAmountToPay(31)).to eq(11)
       expect(calculateAmountToPay(20)).to eq(8)
   end

   it 'charges £3 per box of 5' do
    expect(calculateAmountToPay(5)).to eq(3)
    expect(calculateAmountToPay(10)).to eq(6)
   end

   it 'charges £5 for each group of 3 boxes of 5 cakes' do
    expect(calculateAmountToPay(15)).to eq(5)
    expect(calculateAmountToPay(30)).to eq(10)
   end


    
end