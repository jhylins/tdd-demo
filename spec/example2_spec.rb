require './lib/example2.rb'

describe 'checkout' do
    it 'charges £1 for individual cakes' do
        expect(checkout(1)).to eq(1)
        expect(checkout(2)).to eq(2)
    end

    it 'gives a 3 for 2 offer on individual cakes' do
        expect(checkout(3)).to eq(2)
    end

    it 'applies the 3 for 2 offer and then charges £1 each for any extra cakes' do
        expect(checkout(4)).to eq(3)
    end

    it 'charges £3 for a box of 5 cakes' do
        expect(checkout(5)).to eq(3)
        expect(checkout(10)).to eq(6)
    end

    it 'charges £3 for each box of 5, £2 for each further group of 3 
    and then £1 for extra individual cakes' do
        expect(checkout(6)).to eq(4)
        expect(checkout(7)).to eq(5)
        expect(checkout(8)).to eq(5)
        expect(checkout(9)).to eq(6)
    end
end