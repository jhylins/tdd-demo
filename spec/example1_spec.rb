require './lib/example1.rb'

describe 'checkout' do
    it 'charges £1 for individual cakes' do
        expect(checkout(1)).to eq(1)
        expect(checkout(2)).to eq(2)
    end
    
    it 'charges £0 for 0 cakes' do
        expect(checkout(0)).to eq(0)
    end

    it 'charges £0 for an invalid input' do
        expect(checkout('a')).to eq(0)
    end

    it 'charges 3 for 2 individual cakes' do
        expect(checkout(3)).to eq(2)
        expect(checkout(4)).to eq(3)
    end

    it 'charges £3 for boxes of 5 cakes' do
        expect(checkout(5)).to eq(3)
    end

    it 'applies 3 for 2 rules to remaining individual cakes if more than a box of 5 bought' do
        expect(checkout(6)).to eq(4)
        expect(checkout(8)).to eq(5)
        expect(checkout(9)).to eq(6)
    end

    it 'charges £5 for 3 boxes of 5' do
        expect(checkout(15)).to eq(5)
    end

    it 'applies prices for 3 boxes, then boxes, then 3 for 2s then individuals' do
        expect(checkout(16)).to eq(6)
        expect(checkout(17)).to eq(7)
        expect(checkout(18)).to eq(7)
        expect(checkout(20)).to eq(8)
        expect(checkout(21)).to eq(9)
        expect(checkout(23)).to eq(10)
        expect(checkout(24)).to eq(11)
    end

end