def checkout(cakes) 
    numberOfBoxesOfFive = cakes / 5
    remainderAfterBoxes = cakes % 5
    numberOfThreeForTwos = remainderAfterBoxes / 3
    remainderAfterThreeForTwos = remainderAfterBoxes % 3
    return (3 * numberOfBoxesOfFive) + (2 * numberOfThreeForTwos) + remainderAfterThreeForTwos
end