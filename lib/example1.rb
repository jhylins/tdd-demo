def checkout(cakes)
    if !(cakes.is_a? Integer) || cakes == 0 
        return 0
    end

    groupsOfThreeBoxesOfFive = cakes / 15
    remainderAfterGroupsOfBoxes = cakes % 15
    boxesOfFive = remainderAfterGroupsOfBoxes / 5
    remainderAfterBoxesOfFive = remainderAfterGroupsOfBoxes % 5
    threeForTwos = remainderAfterBoxesOfFive / 3
    remainderAfterThreeForTwos = remainderAfterBoxesOfFive % 3

    return (5 * groupsOfThreeBoxesOfFive) + (3 * boxesOfFive) + (2 * threeForTwos) + remainderAfterThreeForTwos
    

    # cost = 0
    # remainder = cakes

    # if cakes >= 5
    #     # £3 times number of boxes of 5, then pass remainder to 3 for 2 logic
    #     cost = cost + (3 * cakes / 5)
    #     remainder = cakes % 5
    # end

    # if remainder >= 3
    #     # £2 times number of 3 for 2s, plus £1 times the remainder
    #     return cost +  (2 * remainder / 3 + remainder % 3)
    # else
    #     return cost + remainder
    # end

    
end