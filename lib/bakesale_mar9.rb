def calculateAmountToPay(cakes)

    groupsOf15 = cakes / 15
    remainderAfterGroupsOf15 = cakes % 15

    boxesOf5 = remainderAfterGroupsOf15 / 5
    remainderAfterBoxesOf5 = remainderAfterGroupsOf15 % 5

    groupsOf3 = remainderAfterBoxesOf5 / 3
    remainderAfterGroupsOf3 = remainderAfterBoxesOf5 % 3

    return (groupsOf15 * 5) + (boxesOf5 * 3) + (groupsOf3 * 2) + remainderAfterGroupsOf3

end