# Demonstation of Test Driven Development

This project is being used to demonstrate Test Driven Development (TDD). 

## How to Install

Note: You may need to use administrator privilages to run these commands.

- Install Ruby on your machine.
- Install bundler.

```console
$ gem install bundler
```

- Clone this project from github.
- Install the required gems for this project (you may need to run this with sudo prefix)

```console
$ bundle
```

## How to Test

- Run rspec from your command line

```console
$ rspec
```

## Requirements

The requirements which support this project are in a separate presentation.
